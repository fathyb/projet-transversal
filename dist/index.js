(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
var index_1 = require('./renderer/index');
var PerspectiveCamera = THREE.PerspectiveCamera;
var size = Math.min(window.innerWidth, window.innerHeight);
var showPage = false;
var renderer = new index_1.CSS3DRenderer(), canvas = renderer.domElement, camera = new PerspectiveCamera(40, 1, 100, 10000), scene = new THREE.Scene(), cube = new index_1.CSS3DCube(500, '#main-cube .cube-face'), radius = 2000, speed = 20;
camera.position.z = radius;
camera.position.y = 0;
renderer.setSize(size, size);
canvas.style.position = 'absolute';
document.getElementById('container').appendChild(canvas);
window.addEventListener('resize', function () {
    size = Math.min(window.innerWidth, window.innerHeight);
    renderer.setSize(size, size);
    render();
});
canvas.addEventListener('mousemove', function (e) {
    var factor = (e.clientX / canvas.clientWidth - .5) * 2 * speed;
    camera.position.x = scene.position.x + radius * Math.cos(factor);
    camera.position.z = scene.position.z + radius * Math.sin(factor);
    camera.lookAt(scene.position);
    render();
});
scene.add(camera);
scene.add(cube);
cube.children.forEach(function (object) {
    var element = object.element;
    element.classList.add('cube-face');
    element.addEventListener('click', function () {
        console.log('Showing element', element);
        render();
    });
});
cube.rotation.x = Math.PI / 5.1;
cube.rotation.y = Math.PI / 4;
window['camera'] = camera;
window['render'] = render;
console.log(cube, camera, renderer, render);
function render() {
    requestAnimationFrame(function () {
        renderer.render(scene, camera);
    });
}
render();

},{"./renderer/index":3}],2:[function(require,module,exports){
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var object_1 = require('./object');
var CSS3DCube = (function (_super) {
    __extends(CSS3DCube, _super);
    function CSS3DCube(size, selector) {
        var _this = this;
        if (size === void 0) { size = 100; }
        _super.call(this);
        var d = size / 2, r = Math.PI / 2, faces = 6;
        var cubes = [];
        var pos = [
            [d, 0, 0],
            [-d, 0, 0],
            [0, d, 0],
            [0, -d, 0],
            [0, 0, d],
            [0, 0, -d]
        ];
        var rot = [
            [0, r, 0],
            [0, -r, 0],
            [-r, 0, 0],
            [r, 0, 0],
            [0, 0, 0],
            [0, 0, 0]
        ];
        if (selector)
            cubes = Array.prototype.slice.call(document.querySelectorAll(selector));
        else
            while (cubes.length < faces)
                cubes.push(document.createElement('div'));
        cubes.forEach(function (cube, i) {
            var object = new object_1.CSS3DObject(cube);
            cube.style.width = size + "px";
            cube.style.height = size + "px";
            object.position.fromArray(pos[i]);
            object.rotation.fromArray(rot[i]);
            if (i == cubes.length - 1)
                object.scale.set(-1, 1, 1);
            _this.add(object);
        });
    }
    CSS3DCube.prototype.each = function (callback) {
        this.children.forEach(function (object, idx) { return callback(object.element, idx); });
    };
    return CSS3DCube;
}(THREE.Object3D));
exports.CSS3DCube = CSS3DCube;

},{"./object":4}],3:[function(require,module,exports){
"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require('./renderer'));
__export(require('./object'));
__export(require('./sprite'));
__export(require('./cube'));

},{"./cube":2,"./object":4,"./renderer":5,"./sprite":6}],4:[function(require,module,exports){
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var utils_1 = require('../utils');
var CSS3DObject = (function (_super) {
    __extends(CSS3DObject, _super);
    function CSS3DObject(element) {
        var _this = this;
        _super.call(this);
        this.element = element;
        this.element.style.position = 'absolute';
        this.addEventListener('removed', function () {
            if (_this.element.parentNode != null)
                _this.element.parentNode.removeChild(_this.element);
        });
    }
    CSS3DObject.prototype.render = function (renderer, camera) {
        var style = this.getStyle(renderer.matrix, camera), element = this.element;
        if (!this._style || this._style !== style)
            element.style[utils_1.Utils.cssPrefix('transform')] = this._style = style;
        if (element.parentNode !== renderer.camElement)
            renderer.camElement.appendChild(element);
    };
    CSS3DObject.prototype.getStyle = function (matrix, camera) {
        return utils_1.Utils.getObjectCSSMatrix(this.matrixWorld);
    };
    return CSS3DObject;
}(THREE.Object3D));
exports.CSS3DObject = CSS3DObject;

},{"../utils":7}],5:[function(require,module,exports){
/// <reference path='../../typings/browser.d.ts'/>
"use strict";
var utils_1 = require('./../utils');
var object_1 = require('./object');
var CSS3DRenderer = (function () {
    function CSS3DRenderer() {
        this.domElement = document.createElement('div');
        this.camElement = document.createElement('div');
        this.matrix = new THREE.Matrix4();
        this._cache = {
            objects: {},
            camera: {
                fov: 0, style: ''
            }
        };
        this.domElement.style.overflow = 'hidden';
        this.domElement.style[utils_1.Utils.cssPrefix('transformStyle')] = 'preserve-3d';
        this.camElement.style[utils_1.Utils.cssPrefix('transformStyle')] = 'preserve-3d';
        this.domElement.appendChild(this.camElement);
    }
    CSS3DRenderer.prototype.setClearColor = function () { };
    CSS3DRenderer.prototype.setSize = function (width, height) {
        this._width = width;
        this._height = height;
        this._halfWidth = width / 2;
        this._halfHeight = height / 2;
        this.domElement.style.width =
            this.camElement.style.width = width + "px";
        this.domElement.style.height =
            this.camElement.style.height = height + "px";
    };
    CSS3DRenderer.prototype.getSize = function () {
        return {
            width: this._width,
            height: this._height
        };
    };
    CSS3DRenderer.prototype.render = function (scene, camera) {
        var fov = .5 / Math.tan(THREE.Math.degToRad(camera['fov'] * .5)) * this._height, cache = this._cache.camera;
        if (cache.fov !== fov) {
            cache.fov = fov;
            this.domElement.style[utils_1.Utils.cssPrefix('perspective')] = fov + "px";
        }
        scene.updateMatrixWorld(false);
        if (camera.parent === null)
            camera.updateMatrixWorld(false);
        camera.matrixWorldInverse.getInverse(camera.matrixWorld);
        var style = [
            ("translate3d(0, 0, " + fov + "px)"),
            utils_1.Utils.getCameraCSSMatrix(camera.matrixWorldInverse),
            ("translate3d(" + this._halfWidth + "px, " + this._halfHeight + "px, 0)")
        ].join(' ');
        if (cache.style !== style)
            cache.style = this.camElement.style[utils_1.Utils.cssPrefix('transform')] = style;
        this.renderObject(scene, camera);
    };
    CSS3DRenderer.prototype.renderObject = function (object, camera) {
        if (object instanceof object_1.CSS3DObject)
            object.render(this, camera);
        for (var _i = 0, _a = object.children; _i < _a.length; _i++) {
            var children = _a[_i];
            this.renderObject(children, camera);
        }
    };
    return CSS3DRenderer;
}());
exports.CSS3DRenderer = CSS3DRenderer;

},{"./../utils":7,"./object":4}],6:[function(require,module,exports){
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var utils_1 = require('../utils');
var object_1 = require('./object');
var CSS3DSprite = (function (_super) {
    __extends(CSS3DSprite, _super);
    function CSS3DSprite(element) {
        _super.call(this, element);
        this.element = element;
    }
    CSS3DSprite.prototype.getStyle = function (matrix, camera) {
        matrix.copy(camera.matrixWorldInverse);
        matrix.transpose();
        matrix.copyPosition(this.matrixWorld);
        matrix.scale(this.scale);
        matrix.elements[3] = 0;
        matrix.elements[7] = 0;
        matrix.elements[11] = 0;
        matrix.elements[15] = 1;
        return utils_1.Utils.getObjectCSSMatrix(matrix);
    };
    return CSS3DSprite;
}(object_1.CSS3DObject));
exports.CSS3DSprite = CSS3DSprite;

},{"../utils":7,"./object":4}],7:[function(require,module,exports){
//const math = require('mathjs')
//const numeric: NumericJS = require('numeric')
"use strict";
/*interface NumericJS {
    svd: Function
}*/
var Utils;
(function (Utils) {
    Utils.prefix = [
        'Webkit', 'Moz', 'o'
    ].filter(function (_prefix) {
        return cssPrefix('transform', _prefix) in document.createElement('div').style;
    })[0];
    function cssPrefix(prop, _prefix) {
        if (_prefix === void 0) { _prefix = Utils.prefix; }
        if (!_prefix)
            return prop;
        else
            return "" + _prefix + capitalize(prop);
    }
    Utils.cssPrefix = cssPrefix;
    function arrayToMatrix4(array) {
        var matrix = new THREE.Matrix4();
        matrix.fromArray(array);
        return matrix;
    }
    Utils.arrayToMatrix4 = arrayToMatrix4;
    function getCSSMatrix(matrix, invert) {
        var elements = matrix.elements;
        if (invert)
            elements = invertElements(elements, invert);
        return "matrix3d(" + map(elements, function (n) { return Utils.epsilon(n); }).join(',') + ")";
    }
    Utils.getCSSMatrix = getCSSMatrix;
    function map(arr) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (arr.map)
            return arr.map.apply(arr, args);
        else
            return (_a = Array.prototype.map).call.apply(_a, [arr].concat(args));
        var _a;
    }
    Utils.map = map;
    function invertElements(elements, fn) {
        return map(elements, function (n, i) { return fn(i) ? n * -1 : n; });
    }
    Utils.invertElements = invertElements;
    function getCameraCSSMatrix(matrix) {
        return getCSSMatrix(matrix, function (i) { return !((i - 1) % 4); });
    }
    Utils.getCameraCSSMatrix = getCameraCSSMatrix;
    function getObjectCSSMatrix(matrix) {
        return "translate3d(-50%, -50%, 0) " + getCSSMatrix(matrix, function (i) { return i > 3 && i < 8; });
    }
    Utils.getObjectCSSMatrix = getObjectCSSMatrix;
    function capitalize(str) {
        return str.replace(/(?:^|\s)\S/g, function (a) { return a.toUpperCase(); });
    }
    Utils.capitalize = capitalize;
    function epsilon(value) {
        return Math.abs(value) < Number.EPSILON ? 0 : value;
    }
    Utils.epsilon = epsilon;
    function transform3x3Matrix(mat) {
        return [
            mat[0], mat[1], 0, mat[2],
            mat[3], mat[4], 0, mat[5],
            0, 0, 1, 0,
            mat[6], mat[7], 0, mat[8]
        ];
    }
    Utils.transform3x3Matrix = transform3x3Matrix;
    function getDefaultIntrinsic(width, height, z) {
        return [[z, 0, width / 2],
            [0, z, height / 2],
            [0, 0, 1]];
    }
    Utils.getDefaultIntrinsic = getDefaultIntrinsic;
})(Utils = exports.Utils || (exports.Utils = {}));

},{}]},{},[1]);
