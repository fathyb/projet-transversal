"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var object_1 = require('./object');
var CSS3DCube = (function (_super) {
    __extends(CSS3DCube, _super);
    function CSS3DCube(size, selector) {
        var _this = this;
        if (size === void 0) { size = 100; }
        _super.call(this);
        var d = size / 2, r = Math.PI / 2, faces = 6;
        var cubes;
        var pos = [
            [d, 0, 0],
            [-d, 0, 0],
            [0, d, 0],
            [0, -d, 0],
            [0, 0, d],
            [0, 0, -d]
        ];
        var rot = [
            [0, r, 0],
            [0, -r, 0],
            [-r, 0, 0],
            [r, 0, 0],
            [0, 0, 0],
            [0, 0, 0]
        ];
        if (selector)
            cubes = Array.prototype.slice.call(document.querySelectorAll(selector));
        else
            while (cubes.length < faces)
                cubes.push(document.createElement('div'));
        cubes.forEach(function (cube, i) {
            var object = new object_1.CSS3DObject(cube);
            cube.style.width = size + "px";
            cube.style.height = size + "px";
            object.position.fromArray(pos[i]);
            object.rotation.fromArray(rot[i]);
            if (i == cubes.length - 1)
                object.scale.set(-1, 1, 1);
            _this.add(object);
        });
    }
    CSS3DCube.prototype.each = function (callback) {
        this.children.forEach(function (object, idx) { return callback(object.element, idx); });
    };
    return CSS3DCube;
}(THREE.Object3D));
exports.CSS3DCube = CSS3DCube;
