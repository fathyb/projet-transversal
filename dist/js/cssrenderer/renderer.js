/// <reference path='../../typings/browser.d.ts'/>
"use strict";
var utils_1 = require('./../utils');
var object_1 = require('./object');
var CSS3DRenderer = (function () {
    function CSS3DRenderer() {
        this.domElement = document.createElement('div');
        this.camElement = document.createElement('div');
        this.matrix = new THREE.Matrix4();
        this._cache = {
            objects: {},
            camera: {
                fov: 0, style: ''
            }
        };
        this.domElement.style.overflow = 'hidden';
        this.domElement.style[utils_1.Utils.cssPrefix('transformStyle')] = 'preserve-3d';
        this.camElement.style[utils_1.Utils.cssPrefix('transformStyle')] = 'preserve-3d';
        this.domElement.appendChild(this.camElement);
    }
    CSS3DRenderer.prototype.setClearColor = function () { };
    CSS3DRenderer.prototype.setSize = function (width, height) {
        this._width = width;
        this._height = height;
        this._halfWidth = width / 2;
        this._halfHeight = height / 2;
        this.domElement.style.width =
            this.camElement.style.width = width + "px";
        this.domElement.style.height =
            this.camElement.style.height = height + "px";
    };
    CSS3DRenderer.prototype.getSize = function () {
        return {
            width: this._width,
            height: this._height
        };
    };
    CSS3DRenderer.prototype.render = function (scene, camera) {
        var fov = .5 / Math.tan(THREE.Math.degToRad(camera.fov * .5)) * this._height, cache = this._cache.camera;
        if (cache.fov !== fov) {
            cache.fov = fov;
            this.domElement.style[utils_1.Utils.cssPrefix('perspective')] = fov + "px";
        }
        scene.updateMatrixWorld(false);
        if (camera.parent === null)
            camera.updateMatrixWorld(false);
        camera.matrixWorldInverse.getInverse(camera.matrixWorld);
        var style = [
            ("translate3d(0, 0, " + fov + "px)"),
            utils_1.Utils.getCameraCSSMatrix(camera.matrixWorldInverse),
            ("translate3d(" + this._halfWidth + "px, " + this._halfHeight + "px, 0)")
        ].join(' ');
        if (cache.style !== style)
            cache.style = this.camElement.style[utils_1.Utils.cssPrefix('transform')] = style;
        this.renderObject(scene, camera);
    };
    CSS3DRenderer.prototype.renderObject = function (object, camera) {
        if (object instanceof object_1.CSS3DObject)
            object.render(this, camera);
        for (var _i = 0, _a = object.children; _i < _a.length; _i++) {
            var children = _a[_i];
            this.renderObject(children, camera);
        }
    };
    return CSS3DRenderer;
}());
exports.CSS3DRenderer = CSS3DRenderer;
