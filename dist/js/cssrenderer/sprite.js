"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var utils_1 = require('../utils');
var object_1 = require('./object');
var CSS3DSprite = (function (_super) {
    __extends(CSS3DSprite, _super);
    function CSS3DSprite(element) {
        _super.call(this, element);
        this.element = element;
    }
    CSS3DSprite.prototype.getStyle = function (matrix, camera) {
        matrix.copy(camera.matrixWorldInverse);
        matrix.transpose();
        matrix.copyPosition(this.matrixWorld);
        matrix.scale(this.scale);
        matrix.elements[3] = 0;
        matrix.elements[7] = 0;
        matrix.elements[11] = 0;
        matrix.elements[15] = 1;
        return utils_1.Utils.getObjectCSSMatrix(matrix);
    };
    return CSS3DSprite;
}(object_1.CSS3DObject));
exports.CSS3DSprite = CSS3DSprite;
