"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var utils_1 = require('../utils');
var HomographyCamera = (function (_super) {
    __extends(HomographyCamera, _super);
    function HomographyCamera(fov, width, height, far, near) {
        _super.call(this, fov, width / height, far, near);
        this.fov = fov;
        this.orthographicProjection = new THREE.Matrix4();
        this.orthographicProjection.makeOrthographic(0, width, 0, height, 5, -5);
        this.intrinsic = utils_1.Utils.getDefaultIntrinsic(width, height, fov);
    }
    return HomographyCamera;
}(THREE.PerspectiveCamera));
exports.HomographyCamera = HomographyCamera;
