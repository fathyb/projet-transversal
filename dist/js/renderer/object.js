"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var utils_1 = require('../utils');
var CSS3DObject = (function (_super) {
    __extends(CSS3DObject, _super);
    function CSS3DObject(element) {
        var _this = this;
        _super.call(this);
        this.element = element;
        this.element.style.position = 'absolute';
        this.addEventListener('removed', function () {
            if (_this.element.parentNode != null)
                _this.element.parentNode.removeChild(_this.element);
        });
    }
    CSS3DObject.prototype.render = function (renderer, camera) {
        var style = this.getStyle(renderer.matrix, camera), element = this.element;
        if (!this._style || this._style !== style)
            element.style[utils_1.Utils.cssPrefix('transform')] = this._style = style;
        if (element.parentNode !== renderer.camElement)
            renderer.camElement.appendChild(element);
    };
    CSS3DObject.prototype.getStyle = function (matrix, camera) {
        return utils_1.Utils.getObjectCSSMatrix(this.matrixWorld);
    };
    return CSS3DObject;
}(THREE.Object3D));
exports.CSS3DObject = CSS3DObject;
