"use strict";
var UBRelayController = (function () {
    function UBRelayController() {
        this._listeners = {};
    }
    UBRelayController.prototype.emit = function (event, data) {
        if (this._listeners[event])
            this._listeners[event].forEach(function (listener) { return listener(data); });
    };
    UBRelayController.prototype.on = function (event, listener) {
        if (!(event in this._listeners))
            this._listeners[event] = [listener];
        else
            this._listeners[event].push(listener);
    };
    UBRelayController.prototype.off = function (event, listener) {
        if (event in this._listeners)
            if (!listener)
                this._listeners[event] = null;
            else
                this.removeListener(event, listener);
    };
    UBRelayController.prototype.removeListener = function (event, listener) {
        var listeners = this._listeners[event], index = listeners.indexOf(listener);
        if (index != -1)
            listeners.splice(index, 1);
    };
    return UBRelayController;
}());
exports.UBRelayController = UBRelayController;
