//const math = require('mathjs')
//const numeric: NumericJS = require('numeric')
"use strict";
/*interface NumericJS {
    svd: Function
}*/
var Utils;
(function (Utils) {
    Utils.prefix = [
        'Webkit', 'Moz', 'o'
    ].filter(function (_prefix) {
        return cssPrefix('transform', _prefix) in document.createElement('div').style;
    })[0];
    function cssPrefix(prop, _prefix) {
        if (_prefix === void 0) { _prefix = Utils.prefix; }
        if (!_prefix)
            return prop;
        else
            return "" + _prefix + capitalize(prop);
    }
    Utils.cssPrefix = cssPrefix;
    function arrayToMatrix4(array) {
        var matrix = new THREE.Matrix4();
        matrix.fromArray(array);
        return matrix;
    }
    Utils.arrayToMatrix4 = arrayToMatrix4;
    function getCSSMatrix(matrix, invert) {
        var elements = matrix.elements;
        if (invert)
            elements = invertElements(elements, invert);
        return "matrix3d(" + map(elements, function (n) { return Utils.epsilon(n); }).join(',') + ")";
    }
    Utils.getCSSMatrix = getCSSMatrix;
    function map(arr) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (arr.map)
            return arr.map.apply(arr, args);
        else
            return (_a = Array.prototype.map).call.apply(_a, [arr].concat(args));
        var _a;
    }
    Utils.map = map;
    function invertElements(elements, fn) {
        return map(elements, function (n, i) { return fn(i) ? n * -1 : n; });
    }
    Utils.invertElements = invertElements;
    function getCameraCSSMatrix(matrix) {
        return getCSSMatrix(matrix, function (i) { return !((i - 1) % 4); });
    }
    Utils.getCameraCSSMatrix = getCameraCSSMatrix;
    function getObjectCSSMatrix(matrix) {
        return "translate3d(-50%, -50%, 0) " + getCSSMatrix(matrix, function (i) { return i > 3 && i < 8; });
    }
    Utils.getObjectCSSMatrix = getObjectCSSMatrix;
    function capitalize(str) {
        return str.replace(/(?:^|\s)\S/g, function (a) { return a.toUpperCase(); });
    }
    Utils.capitalize = capitalize;
    function epsilon(value) {
        return Math.abs(value) < Number.EPSILON ? 0 : value;
    }
    Utils.epsilon = epsilon;
    function transform3x3Matrix(mat) {
        return [
            mat[0], mat[1], 0, mat[2],
            mat[3], mat[4], 0, mat[5],
            0, 0, 1, 0,
            mat[6], mat[7], 0, mat[8]
        ];
    }
    Utils.transform3x3Matrix = transform3x3Matrix;
    function getDefaultIntrinsic(width, height, z) {
        return [[z, 0, width / 2],
            [0, z, height / 2],
            [0, 0, 1]];
    }
    Utils.getDefaultIntrinsic = getDefaultIntrinsic;
})(Utils = exports.Utils || (exports.Utils = {}));
