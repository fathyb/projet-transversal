const gulp = require('gulp'),
	  jade = require('gulp-jade'),
	  sass = require('gulp-sass'),
	  watch = require('gulp-watch'),
	  batch = require('gulp-batch'),
	  browserify = require('browserify'),
	  uglify = require('gulp-uglify'),
	  gutil = require('gulp-util'),
	  sourcemaps = require('gulp-sourcemaps'),
	  source = require('vinyl-source-stream'),
	  buffer = require('vinyl-buffer'),
	  ts = require('gulp-typescript'),
	  rename = require('gulp-rename')

const tsProject = ts.createProject({
	module: "commonjs",
	target: "es5",
	sourceMap: true
})

const paths = {
	html: 'views/**/*.jade',
	css : 'sass/**/*.sass',
	js  : 'ts/**/*.ts',
	lib : 'lib/**/*'
}


gulp.task('build', ['css', 'html', 'js', 'lib'])
	.task('css',   ['sass'])
	.task('html',  ['templates'])
	.task('js',    ['uglify'])
	.task('lib', () =>
		gulp.src(paths.lib)
			.pipe(gulp.dest('dist/lib'))
	).task('watch', () =>
		Object.keys(paths).forEach(task =>
			watch(paths[task], batch((e, done) =>
				gulp.start(task, done)
			))
		)
	).task('templates', () =>
		gulp.src(paths.html)
			.pipe(jade().on('error', gutil.log))
			.pipe(gulp.dest('dist'))
	).task('sass', () =>
		gulp.src(paths.css)
			.pipe(sass().on('error', gutil.log))
			.pipe(gulp.dest('dist/css'))
	).task('typescript', () =>
		gulp.src(['typings/browser.d.ts', paths.js])
			.pipe(ts(tsProject))
			.js
			.pipe(gulp.dest('dist/js'))
			.on('error', gutil.log)
	).task('uglify', ['browserify'], () =>
		gulp.src('dist/index.js')
			.pipe(rename({
				suffix: '.min'
			}))
			.pipe(sourcemaps.init({ loadMaps: true }))
			.pipe(uglify())
			.pipe(sourcemaps.write('./'))
			.pipe(gulp.dest('dist'))
			.on('error', gutil.log)
	).task('browserify', ['typescript'], () =>
		browserify({
			entries: 'dist/js/index.js'
		})  .bundle()
			.pipe(source('index.js'))
			.pipe(buffer())
			.pipe(gulp.dest('dist'))
			.on('error', gutil.log)
	)