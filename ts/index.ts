import {CSS3DRenderer, CSS3DCube} from './renderer/index'
import {Utils} from './utils'
import PerspectiveCamera = THREE.PerspectiveCamera

interface WebKit {
	messageHandlers: any
}
declare var webkit: WebKit


let size = Math.min(window.innerWidth, window.innerHeight)
let showPage = false

const renderer = new CSS3DRenderer(),
	  canvas   = renderer.domElement,
	  camera   = new PerspectiveCamera(40, 1, 100, 10000),
	  scene    = new THREE.Scene(),
	  cube     = new CSS3DCube(500, '#main-cube .cube-face'),
	  radius   = 2000,
	  speed    = 20

camera.position.z = radius
camera.position.y = 0

renderer.setSize(size, size)
canvas.style.position = 'absolute'
document.getElementById('container').appendChild(canvas)

window.addEventListener('resize', () => {
	size = Math.min(window.innerWidth, window.innerHeight)

	renderer.setSize(size, size)
	render()
})

canvas.addEventListener('mousemove', e => {
	const factor = (e.clientX / canvas.clientWidth - .5) * 2 * speed

	camera.position.x = scene.position.x + radius * Math.cos(factor)
	camera.position.z = scene.position.z + radius * Math.sin(factor)
	camera.lookAt(scene.position)

	render()
})

scene.add(camera)
scene.add(cube)

cube.children.forEach(object => {
	const element = object.element

	element.classList.add('cube-face')

	element.addEventListener('click', () => {
		console.log('Showing element', element)

		render()
	})
})



cube.rotation.x = Math.PI / 5.1
cube.rotation.y = Math.PI / 4

window['camera'] = camera
window['render'] = render
console.log(cube, camera, renderer, render)


function render() {
	requestAnimationFrame(() => {
		renderer.render(scene, camera)
	})
}

render()
