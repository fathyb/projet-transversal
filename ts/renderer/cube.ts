import {CSS3DObject} from './object'

export class CSS3DCube extends THREE.Object3D {
	public children: CSS3DObject[]

	constructor(size: number = 100, selector?: string) {
		super()

		const d = size / 2,
			  r = Math.PI / 2,
			  faces = 6
		let cubes: HTMLElement[] = []

		const pos = [
			[ d, 0, 0],
			[-d, 0, 0],
			[0,  d, 0],
			[0, -d, 0],
			[0, 0,  d],
			[0, 0, -d]
		]
		const rot = [
			[0,  r, 0],
			[0, -r, 0],
			[-r, 0, 0],
			[ r, 0, 0],
			[0,  0, 0],
			[0,  0, 0]
		]

		if(selector)
			cubes = Array.prototype.slice.call(document.querySelectorAll(selector))
		else
			while(cubes.length < faces)
				cubes.push(document.createElement('div'))

		cubes.forEach((cube, i) => {
			const object = new CSS3DObject(cube)

			cube.style.width = `${size}px`
			cube.style.height = `${size}px`

			object.position.fromArray(pos[i])
			object.rotation.fromArray(rot[i])

			if(i == cubes.length - 1)
				object.scale.set(-1, 1, 1)

			this.add(object)
		})
	}

	each(callback: (object: HTMLElement, idx: number) => void): void {
		this.children.forEach((object, idx) => callback(object.element, idx))
	}
}