import {Utils} from '../utils'

export class HomographyCamera extends THREE.PerspectiveCamera {
	public orthographicProjection: THREE.Matrix4
	public intrinsic: number[][]
	public fov: number

	constructor(fov: number, width: number, height: number, far: number, near: number) {
		super(fov, width / height, far, near)
		this.fov = fov
		this.orthographicProjection = new THREE.Matrix4()
		this.orthographicProjection.makeOrthographic(0, width, 0, height, 5, -5)
		this.intrinsic = Utils.getDefaultIntrinsic(width, height, fov)
	}
/*
	public fromHomography(homography: number[]): void {
		var t = Utils.extrapolate3DMatrix(homography, this.intrinsic, true, true)

		document.getElementById('test-cube').style.transform = 'matrix3d(' + [
				t[0][0], t[0][1], t[0][2], t[0][3],
				t[1][0], t[1][1], t[1][2], t[1][3],
				0,       0,       1,       0,
				t[2][0], t[2][1], t[2][2], t[2][3]
			].join(',') + ')'
		this.projectionMatrix.set(
			t[0][0], t[0][1], t[0][2], t[0][3],
			t[1][0], t[1][1], t[1][2], t[1][3],
				 0,       0,       1,       0,
			t[2][0], t[2][1], t[2][2], t[2][3])

		this.projectionMatrix.multiplyMatrices(this.orthographicProjection, this.projectionMatrix)

		console.log('Projection matrix applied :', t)
		//this.updateProjectionMatrix()
	}*/
}