import {Utils} from '../utils'
import {CSS3DRenderer} from './renderer'

export class CSS3DObject extends THREE.Object3D {
	private _style: string

	constructor(public element: HTMLElement) {
		super()

		this.element.style.position = 'absolute'
		this.addEventListener('removed', () => {
			if(this.element.parentNode != null)
				this.element.parentNode.removeChild(this.element)
		})
	}

	public render(renderer: CSS3DRenderer, camera: THREE.Camera): void {
		const style   = this.getStyle(renderer.matrix, camera),
			element = this.element

		if(!this._style || this._style !== style)
			element.style[Utils.cssPrefix('transform')] = this._style = style

		if(element.parentNode !== renderer.camElement)
			renderer.camElement.appendChild(element)
	}

	protected getStyle(matrix: THREE.Matrix4, camera: THREE.Camera): string {
		return Utils.getObjectCSSMatrix(this.matrixWorld)
	}
}