/// <reference path='../../typings/browser.d.ts'/>

import {Utils} from './../utils'
import {CSS3DObject} from './object'

interface CSS3DRendererCache {
	objects: {}
	camera: {
		fov: number
		style: string
	}
}
interface CSS3DRendererSize {
	width : number
	height: number
}

export class CSS3DRenderer {
	public domElement: HTMLElement = document.createElement('div')
	public camElement: HTMLElement = document.createElement('div')
	public matrix:   THREE.Matrix4 = new THREE.Matrix4()

	private _width : number
	private _height: number
	private _halfWidth : number
	private _halfHeight: number
	private _cache: CSS3DRendererCache = {
		objects: {},
		camera: {
			fov: 0, style: ''
		}
	}

	constructor() {
		this.domElement.style.overflow = 'hidden'
		this.domElement.style[Utils.cssPrefix('transformStyle')] = 'preserve-3d'
		this.camElement.style[Utils.cssPrefix('transformStyle')] = 'preserve-3d'
		this.domElement.appendChild(this.camElement)
	}

	public setClearColor(): void {}
	public setSize(width: number, height: number): void {
		this._width  = width
		this._height = height
		this._halfWidth  = width / 2
		this._halfHeight = height / 2

		this.domElement.style.width  =
		this.camElement.style.width  = `${width}px`
		this.domElement.style.height =
		this.camElement.style.height = `${height}px`
	}
	public getSize(): CSS3DRendererSize {
		return {
			width : this._width,
			height: this._height
		}
	}
	public render(scene: THREE.Scene, camera: THREE.Camera): void {
		const fov = .5 / Math.tan(THREE.Math.degToRad(camera['fov'] * .5 )) * this._height,
			  cache = this._cache.camera

		if(cache.fov !== fov) {
			cache.fov = fov
			this.domElement.style[Utils.cssPrefix('perspective')] = `${fov}px`
		}

		scene.updateMatrixWorld(false)

		if(camera.parent === null)
			camera.updateMatrixWorld(false)

		camera.matrixWorldInverse.getInverse(camera.matrixWorld)

		const style = [
			`translate3d(0, 0, ${fov}px)`,
			Utils.getCameraCSSMatrix(camera.matrixWorldInverse),
			`translate3d(${this._halfWidth}px, ${this._halfHeight}px, 0)`
		].join(' ')

		if(cache.style !== style)
			cache.style = this.camElement.style[Utils.cssPrefix('transform')] = style

		this.renderObject(scene, camera)
	}
	protected renderObject(object: THREE.Object3D, camera: THREE.Camera): void {
		if(object instanceof CSS3DObject)
			object.render(this, camera)

		for(let children of object.children)
			this.renderObject(children, camera)
	}
}