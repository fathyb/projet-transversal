import {Utils} from '../utils'
import {CSS3DRenderer} from './renderer'
import {CSS3DObject} from './object'

export class CSS3DSprite extends CSS3DObject {
	constructor(public element: HTMLElement) {
		super(element)
	}

	protected getStyle(matrix: THREE.Matrix4, camera: THREE.Camera): string {
		matrix.copy(camera.matrixWorldInverse)
		matrix.transpose()
		matrix.copyPosition(this.matrixWorld)
		matrix.scale(this.scale)

		matrix.elements[ 3] = 0
		matrix.elements[ 7] = 0
		matrix.elements[11] = 0
		matrix.elements[15] = 1

		return Utils.getObjectCSSMatrix(matrix)
	}
}