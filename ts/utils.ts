//const math = require('mathjs')
//const numeric: NumericJS = require('numeric')

/*interface NumericJS {
	svd: Function
}*/

export module Utils {
	export const prefix: string = [
		'Webkit', 'Moz', 'o'
	].filter(_prefix =>
		cssPrefix('transform', _prefix) in document.createElement('div').style
	)[0]

	export function cssPrefix(prop: string, _prefix: string = prefix): string {
		if(!_prefix)
			return prop
		else
			return `${_prefix}${capitalize(prop)}`
	}

	export function arrayToMatrix4(array: number[]): THREE.Matrix4 {
		const matrix = new THREE.Matrix4()

		matrix.fromArray(array)

		return matrix
	}
	export function getCSSMatrix(matrix: THREE.Matrix4, invert?: (number) => boolean): string {
		let elements = matrix.elements

		if(invert)
			elements = invertElements(elements, invert)

		return `matrix3d(${
			map(elements, n => Utils.epsilon(n)).join(',')
		})`
	}
	export function map(arr: any, ...args): any {
		if(arr.map)
			return arr.map(...args)
		else
			return Array.prototype.map.call(arr, ...args)
	}
	export function invertElements(elements: Float32Array, fn: (number) => boolean): Float32Array {
		return map(elements, (n, i) => fn(i) ? n * -1 : n)
	}
	export function getCameraCSSMatrix(matrix: THREE.Matrix4): string {
		return getCSSMatrix(matrix, i => !((i - 1) % 4))
	}
	export function getObjectCSSMatrix(matrix: THREE.Matrix4): string {
		return `translate3d(-50%, -50%, 0) ${
			getCSSMatrix(matrix, i => i > 3 && i < 8)
		}`
	}

	export function capitalize(str: string): string {
		return str.replace(/(?:^|\s)\S/g, a => a.toUpperCase())
	}
	export function epsilon(value: number): number {
		return Math.abs(value) < Number.EPSILON ? 0 : value
	}

	export function transform3x3Matrix(mat: number[]): number[] {
		return [
			mat[0], mat[1], 0, mat[2],
			mat[3], mat[4], 0, mat[5],
				0 ,     0,  1,     0,
			mat[6], mat[7], 0, mat[8]
		]
	}

	export function getDefaultIntrinsic(width: number, height: number, z: number): number[][] {
		return [[z,	0,  width / 2],
				[0, z, height / 2],
				[0,	0, 1]]
	}
/*

	export function extrapolate3DMatrix(
		homography: any, //(number[][]) | (number[]),
		intrinsic: number[][],
		enforceOrthogonal: boolean,
		preserveHomography: boolean
	) : number[][] {
		//console.log(homography);
		if(homography.length > 3){
			const h = homography
			homography = [
				[h[0], h[1], h[2]],
				[h[3], h[4], h[5]],
				[h[6], h[7], h[8]],
			]
		}

		/*{
			let intrinsic: THREE.Matrix3

			let homography = new THREE.Matrix3()

			let kPrime = homography.getInverse(intrinsic)
			let orientation2d = new THREE.Matrix3()

			//orientation2d.multiplyMatrices(kPrime, homography)

			let components = orientation2d.transpose()


			let v1: THREE.Vector3 = components[0]
			let v2: THREE.Vector3 = components[1]
			let v3: THREE.Vector3 = v2
			let t : THREE.Vector3 = components[2]

			let normFactor = v1.normalize()

			v1.divide(normFactor)
			v2.divide(normFactor)
			 t.divide(normFactor)

			v3.cross(v1)

			let rotationMatrix = new THREE.Matrix3()

			rotationMatrix.set
			rotationMatrix.transpose()
		}

		var Kprime = math.inv(intrinsic);
		var orientation2d = math.multiply(Kprime,homography);
		var components = math.transpose(orientation2d);
		var r1 = components[0];
		var r2 = components[1];
		var t = components[2];

		var normFactor = math.norm(r1);
		r1 = math.divide(r1,normFactor);
		r2 = math.divide(r2,normFactor);
		t = math.divide(t,normFactor);

		var r3 = math.cross(r1,r2);
		var rotMatrix = math.transpose([r1,r2,r3]);

		if(enforceOrthogonal){
			var svd = numeric.svd(rotMatrix);
			rotMatrix = math.multiply(svd.U, math.transpose(svd.V));
		}

		if(preserveHomography){
			var nrcomponents = math.transpose(rotMatrix);
			var r3 = nrcomponents[2];
			rotMatrix = math.transpose([r1,r2,r3]);
		}

		var finalOrientation = math.concat(rotMatrix,math.map(t,function(x){return [x]}));

		return math.multiply(intrinsic,finalOrientation);
	}
*/
}
