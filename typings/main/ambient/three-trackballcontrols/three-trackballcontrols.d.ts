// Compiled using typings@0.6.10
// Source: https://raw.githubusercontent.com/DefinitelyTyped/DefinitelyTyped/dccc81ea05777bfed0307e4123974b682e6b46ab/threejs/three-trackballcontrols.d.ts
// Type definitions for three.js (TrackballControls.js)
// Project: https://github.com/mrdoob/three.js/blob/master/examples/js/controls/TrackballControls.js
// Definitions by: Satoru Kimura <https://github.com/gyohk>
// Definitions: https://github.com/borisyankov/DefinitelyTyped


declare module THREE {
    class TrackballControls extends EventDispatcher {
        constructor(object:Camera, domElement?:HTMLElement);

        object:Camera;
        domElement:HTMLElement;

        // API
        enabled:boolean;
        screen:{ left: number; top: number; width: number; height: number };
        rotateSpeed:number;
        zoomSpeed:number;
        panSpeed:number;
        noRotate:boolean;
        noZoom:boolean;
        noPan:boolean;
        noRoll:boolean;
        staticMoving:boolean;
        dynamicDampingFactor:number;
        minDistance:number;
        maxDistance:number;
        keys:number[];

        position0: THREE.Vector3;
        target0: THREE.Vector3;
        up0: THREE.Vector3;

        update():void;
        reset():void;
        checkDistances():void;
        zoomCamera():void;
        panCamera():void;
        rotateCamera():void;

        handleResize():void;
        handleEvent(event: any):void;
    }
}